
class Country:
	main_region = 13
	def __init__(self, regions):
		self.r = regions
		
	def solve(self):
		m = 1;
		while(1):
			if(self.try_m(m)):
				return m
			m+=1
		
	def try_m(self, m):
		turned_off_regions = 1
		regions = [0] * self.r
		current = 0
		regions[0] = 1
		
		while(turned_off_regions < self.r-1):
			size = 0;
			while(size < m or regions[current%self.r] == 1):
				current+=1
				if(regions[current%self.r] == 1):
					continue
				size+=1

			if(current%self.r == self.main_region-1):
				return 0
				
			turned_off_regions+=1
			regions[current%self.r] = 1
		
		return 1		
	
	

class PowerCrisisSolver:
	def __init__(self, data_file, result_file):
		self.f_data = data_file
		self.f_result = result_file
		self.data = []
		
	def readData(self):
		with open(self.f_data, "r") as file:
			data = file.read();
		N_list = data.splitlines();
		for i in range(len(N_list)):
			if(N_list[i] == "0"):
				break
			self.data.append(int(N_list[i]))
			
	def solve(self):
		self.results = []
		for i in range(len(self.data)):
			c = Country(int(self.data[i]));
			self.results.append(c.solve());
		
	def writeResults(self):
		with open(self.f_result, "w") as file:
		
			for i in range(len(self.results)):
				file.write(str(self.results[i]) + "\n")
			
		

solver = PowerCrisisSolver("data.txt", "result.txt")
solver.readData()
solver.solve()
solver.writeResults()
